const mysql=require('mysql');
secret = (async () => {
    const {secret} = await import('./secret.mjs');
    
    return secret;
})();



var con= secret.then((secretdata)=>{
    
   con= mysql.createConnection({
        host:secretdata.host,
        user:secretdata.username,
        password:secretdata.password,
        database:secretdata.dbInstanceIdentifier
    });
    
    con.connect((err)=>{
        if(err)
        {
            console.log(err);
            console.warn("error in connection")
        }
    });
    
    return con;
   
})


module.exports =con;



