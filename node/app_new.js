const express = require("express");
const sls=require('serverless-http');
var condata = require("./config.js");
var app = express();

app.use(express.json());
    app.get("/", (req, resp) => {
    resp.send("Welcome to Rest API with Mysql");
});

app.get("/getusers",(req, resp) => 
{
  condata.then((con)=>
  {
    con.query("select * from users", (err, result) => {
    if (err) { resp.send("error in api"+err) }
    else { resp.send(result) }
  })
 
})
   
});

app.get("/getuser/:id", (req, resp) => 
{
   condata.then((con)=>
  {
   con.query("select * from users where id=?", req.params.id,(err, result) => {
    if (err) { resp.send("error in api") }
    else { resp.send(result) }
  })
  })
});


app.post("/createuser", (req, resp) => {
   condata.then((con)=>
  {
  const data = req.body;
  con.query("INSERT INTO users SET?", data, (error, results, fields) => {
    if (error) throw error;
    // else{

    //   con.query("select * from users order by id desc limit 1 ", (err, result) => {
    //     if (err) { resp.send("error in api"+err) }
    //     else { resp.send(result) }
    //   })
    // }
    resp.send(req.body)
  })
  })
});


app.put("/updateuser/:id",(req,resp)=>{
   condata.then((con)=>
  {
  const data= [req.body.name,req.body.password,req.body.user_type,req.params.id];
  con.query("UPDATE users SET name = ?, password = ?, user_type = ? WHERE id = ?",
  data,(error,results,fields)=>{
    if(error) throw error;
    resp.send(req.body)
  })
  })
})

app.delete("/deleteuser/:id",(req,resp)=>{
  condata.then((con)=>
  {
  con.query("DELETE FROM users WHERE id = ?",req.params.id,
  (error,results,fields)=>{
    if(error) throw error;
    resp.send("Successfully Deleted Record for Id:"+" "+req.params.id);
  })
  })
 
})

module.exports.server= sls(app);



  

