import {
    SecretsManagerClient,
    GetSecretValueCommand,
  } from "@aws-sdk/client-secrets-manager";
const secret_name = "prod/app/mysql";
    
    const client = new SecretsManagerClient({
      region: "ap-south-1",
    });
    
    let response;
    
    try {
      response = await client.send(
        new GetSecretValueCommand({
          SecretId: secret_name,
          VersionStage: "AWSCURRENT", // VersionStage defaults to AWSCURRENT if unspecified
        })
      );
    } catch (error) {
      // For a list of exceptions thrown, see
      // https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
      throw error;
    }
    
    export const secret = JSON.parse(response.SecretString);
 
  
   



// const sec=getsecret().then((resdata)=>{

//     const data=resdata;
  
//     console.log(data.username);
//     console.log(data.password);

// });

    // Your code goes here


